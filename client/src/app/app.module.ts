import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import { routing } from "./app.routes";
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxEditorModule } from 'ngx-editor';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar.component';
import { NewArticleComponent } from "./newArticle/new-article.component";
import { ArticlesListComponent } from "./articlesList/articles-list.component";
import {ArticlesService} from "./services/articles.service";
import {CategoriesService} from "./services/categories.service";
import {RotateImageComponent} from "./rotateImage/rotateImage.component";
import { CategoryComponent } from "./category/category.component";
import {PaginationComponent} from "./pagination/pagination.component";
import {DetailArticleComponent} from "./detailArticle/detailArticle.component";
import {FormForCommentsComponents} from "./formForComments/formForComments.component";
import {CommentsService} from "./services/comments.service";
import {LoginComponent} from "./login/login.component";
import {AuthenticationService} from "./services/authentication.service";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NewArticleComponent,
    ArticlesListComponent,
    CategoryComponent,
    PaginationComponent,
    DetailArticleComponent,
    FormForCommentsComponents,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    RouterModule,
    routing,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    Ng2ImgMaxModule,
    FontAwesomeModule,
    NgxEditorModule,
    ModalModule.forRoot()
  ],
  providers: [ArticlesService,
              CategoriesService,
              RotateImageComponent,
              CommentsService,
              AuthenticationService],
  bootstrap: [AppComponent],
  entryComponents: [LoginComponent]
})
export class AppModule { }
