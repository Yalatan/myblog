import { Routes, RouterModule } from '@angular/router';
import { NewArticleComponent } from "./newArticle/new-article.component";
import { ArticlesListComponent } from "./articlesList/articles-list.component";
import { CategoryComponent } from "./category/category.component";
import {DetailArticleComponent} from "./detailArticle/detailArticle.component";

const routes: Routes = [
  { path: '',    component: ArticlesListComponent },
  { path: 'articles/new', component: NewArticleComponent },
  { path: 'category/:category', component: CategoryComponent },
  { path: 'articles/:id', component: DetailArticleComponent },
  { path: '**',    component: ArticlesListComponent }

];

export const routing = RouterModule.forRoot(routes);

