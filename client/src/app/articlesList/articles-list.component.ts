import {Component} from '@angular/core';

import {ArticlesService} from "../services/articles.service";
//import EXIF = require("../../../node_modules/exif-js/exif");

import  * as EXIF from '../../../node_modules/exif-js/exif';

@Component({
  templateUrl: "./articles-list.component.html",
  styleUrls: ['./articles-list.component.css']

})
export class ArticlesListComponent {
  articles;
  subscription;
  articlesPerPage = 5;


  constructor (private _articlesService: ArticlesService) {
    this.subscription = this._articlesService.onNewArticle.subscribe(value => {
      this.articles.push(value);
    });
      this.subscription = this._articlesService.articlesOnPage.subscribe(articles => {
          this.articles = articles["articles"];
          //this.getExif();

       })
  }

    // getExif() {
    //     let img = document.getElementById("img");
    //     // EXIF.getData(img, function() {
    //     //     let make = EXIF.getTag(this, "Make");
    //     //     let model = EXIF.getTag(this, "Model");
    //     //
    //     // });
    // }


}
