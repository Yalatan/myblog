import {Component} from '@angular/core';
import {ArticlesService} from "../services/articles.service";

@Component({
    templateUrl: "./category.component.html",
    styleUrls: ['./category.component.css']
})

export class CategoryComponent {
    articlesCategory;
    articles;
    subscription;

    constructor (private _articlesService: ArticlesService) {
        this.subscription = this._articlesService.articlesByCategory.subscribe(articles => {
            this.articles = articles["articles"];
        })
    }



}