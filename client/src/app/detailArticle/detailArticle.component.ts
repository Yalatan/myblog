import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

import {ArticlesService} from "../services/articles.service";


@Component({
    templateUrl: "./detailArticle.component.html",
    styleUrls: ['./detailArticle.component.css']
})

export class DetailArticleComponent implements OnInit {
    article;
    articleId;

    constructor (private _articleService: ArticlesService,
                 private _route: ActivatedRoute) {

    }

    ngOnInit() {
        this._route.params.subscribe(params => {
            this.articleId = params['id'];
            this._articleService.getDetailArticle(this.articleId).subscribe(article => {
                this.article = article;

            } );

        });

    }


}