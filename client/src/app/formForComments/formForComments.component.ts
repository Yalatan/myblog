import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

import {CommentsService} from '../services/comments.service';

@Component({
    selector: "formForComments",
    templateUrl: "./formForComments.component.html"
})

export class FormForCommentsComponents implements OnInit {
    formComments;
    editorConfig = {
        "editable": true,
        "spellcheck": true,
        "height": "100",
        "minHeight": "100",
        "width": "auto",
        "minWidth": "0",
        "translate": "yes",
        "enableToolbar": true,
        "showToolbar": true,
        "placeholder": "",
        "imageEndPoint": "",
        "toolbar": [
            ["bold", "italic", "underline", "strikeThrough"],
            ["fontName", "fontSize", "color"],
            ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
            ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
            ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
            ["link", "unlink"]
        ]
    };

    constructor(private fb: FormBuilder,
    private _commentsService: CommentsService) {}

    ngOnInit() {

        this.formComments = this.fb.group({
            comment: ['', [Validators.required]]
        });
    }

    onSubmit() {
        console.log(this.formComments.value);
        this._commentsService.postComment(this.formComments.value).subscribe()
    }

}