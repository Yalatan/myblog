import {Component, OnInit} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {AuthenticationService} from "../services/authentication.service";


@Component({
    templateUrl: "./login.component.html"

})
export class LoginComponent implements OnInit{
    loginForm: FormGroup;

    constructor (public modalRef: BsModalRef,
                 private formBuilder: FormBuilder,
                 private _authenticationService: AuthenticationService) {}

    ngOnInit () {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    onSubmit() {
        this._authenticationService.postLoginData(this.loginForm.value).subscribe();
    }

}