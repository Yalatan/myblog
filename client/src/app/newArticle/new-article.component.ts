import { Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
//import  * as getOrientedImage from '../../../node_modules/exif-orientation-image/index';
//import { Ng2ImgMaxService } from 'ng2-img-max';


import {ArticlesService} from "../services/articles.service";
import {CategoriesService} from "../services/categories.service";
import {RotateImageComponent} from "../rotateImage/rotateImage.component";
import {createElement} from "../../../node_modules/@angular/core/src/view/element";
import {reject} from "q";


@Component({
  templateUrl: "./new-article.component.html",
  styleUrls: ['./new-article.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class NewArticleComponent implements OnInit {

  articleForm: FormGroup;
  imagesFiles : Array<File> = [];
  readedImages: Array<File> = [];
  mainImageFile;
  mainReadedImage;
  categories;
  errorMessage;
  counterForId = 0;
  canvas;
  blobArray : Array<File> = [];
  blob;


  constructor(private fb: FormBuilder,
              private _articlesService: ArticlesService,
              private _categoriesService: CategoriesService,
              private _router: Router,
              private _rotateImageComponent: RotateImageComponent,
              private elementRef: ElementRef) {

    this.articleForm = this.fb.group({
      categoryName: ['', [Validators.required]],
      articleName: ['', [Validators.required]],
      content: ['', [Validators.required]]

    });
  }


  ngOnInit () {
    this._categoriesService.getCategories().subscribe(categories => {
      this.categories = categories;
    });

  }


   loadCanvas(image) {
       this._rotateImageComponent.load(image, (err, canvas) => {
           if (!err) {
               this.canvas = canvas;
               this.canvas.className = 'canvas';
               this.canvas.toBlob((blob) => {
                   this.blob = blob;
                   this.blobArray.push(blob);
                   console.log(this.blobArray);

               });
                       let previewImage = document.getElementById('previewImage');
                       let content = document.createElement('div');
                       content.className = 'content';
                       content.id = (this.counterForId++).toString();

                       previewImage.appendChild(content);
                       content.appendChild(this.canvas);

                       let closeButton = document.createElement('a');
                       closeButton.className = 'closeButton';
                       closeButton.innerHTML = 'x';
                       content.appendChild(closeButton);


                       let el = document.getElementById(content.id);
                       let childElA = el.getElementsByTagName('a')[0];
                       childElA.addEventListener('click', (el) => this.deleteImage(el, content.id, this.blob));

                       let childElCanvas = el.getElementsByTagName('canvas')[0];
                       childElCanvas.addEventListener('click', (el) => this.chooseMainImage(this.blob, canvas));

           }

       });


   }



  upload(event) {
    for (let i = 0; i < event.target.files.length; i++) {
      let newFile = event.target.files.item(i);
      if (this.imagesFiles.length <= 9) {
          this.imagesFiles.push(newFile);
          this.loadCanvas(newFile);
      }

    }

  }

   chooseMainImage(image, canvas) {

       let containerForMainImage = document.getElementById('mainPicture');
       if (containerForMainImage.hasChildNodes()) {
           containerForMainImage.removeChild(containerForMainImage.childNodes[0]);
       }
       let mainImageDiv = document.createElement('div');
       mainImageDiv.className = 'content';
       mainImageDiv.id = 'main-picture';
       containerForMainImage.appendChild(mainImageDiv);

       let newCanvas = document.createElement('canvas');
       newCanvas.className = 'canvas-main';
       let context = newCanvas.getContext('2d');
       newCanvas.width = canvas.width;
       newCanvas.height = canvas.height;

       context.drawImage(canvas, 0, 0);

       // let mainImageIndex = this.imagesFiles.indexOf(image);
       // this.mainImageFile = this.imagesFiles[mainImageIndex];
       let mainImageIndex = this.blobArray.indexOf(image);
       this.mainImageFile = this.blobArray[mainImageIndex];

       mainImageDiv.appendChild(newCanvas);

       let closeButton = document.createElement('a');
       closeButton.className = 'closeButton';

       closeButton.innerHTML = 'x';
       mainImageDiv.appendChild(closeButton);
       closeButton.addEventListener('click', (el) => this.deleteMainImage());

   }


  deleteImage(el, id, image) {
      let parent =  document.getElementById('previewImage');
      let child = document.getElementById(id);
      parent.removeChild(child);
      //this.imagesFiles.splice(this.imagesFiles.indexOf(image), 1);
      this.blobArray.splice(this.blobArray.indexOf(image), 1);
      console.log(this.blobArray);

  }

    deleteMainImage() {
        let parent =  document.getElementById('mainPicture');
        let child = document.getElementById('main-picture');
        parent.removeChild(child);
        this.mainImageFile = null;
    }

  onSubmit() {

    this._articlesService.postArticle(this.articleForm.value, this.blobArray, this.mainImageFile).subscribe((x => {
        this._router.navigate(['/']);
    }));

  }


}
