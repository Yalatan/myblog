import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import * as _ from 'underscore';

import {ArticlesService} from "../services/articles.service";


@Component({
    selector: "pagination",
    templateUrl: "./pagination.component.html"

})
export class PaginationComponent implements OnInit {
    currentPage;
    articles;
    articlesTotal;
    pagesTotal;
    pages;
    articlesPerPage = 5;
    articlesCategory;

    constructor (private _articlesService: ArticlesService,
                 private _route: ActivatedRoute) {}

    ngOnInit() {
        this.setPage(1);
    }

    setPage(currentPage: number) {
        this.currentPage = currentPage;
        this._route.params.subscribe(params => {
            if(params['category']) {
                this.articlesCategory = params['category'];
                this._articlesService.getArticlesByCategory(this.articlesCategory, currentPage, this.articlesPerPage).subscribe(articles => {
                    this.articles = articles["articles"];
                    this.articlesTotal = articles["articlesTotal"];
                    this.pagesTotal = Math.ceil(this.articlesTotal/this.articlesPerPage);
                    this.calculatePageRange();
                });
            } else {
                this._articlesService.getArticles(currentPage, this.articlesPerPage).subscribe(articles => {
                    this.articles = articles["articles"];
                    this.articlesTotal = articles["articlesTotal"];
                    this.pagesTotal = Math.ceil(this.articlesTotal/this.articlesPerPage);
                    this.calculatePageRange();
                });
            }
        });

    }

    calculatePageRange() {

        let startPage;
        let endPage;
        if (this.pagesTotal <= 10) {
            startPage = 1;
            endPage = this.pagesTotal;
        } else {
            if (this.currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (this.currentPage + 4 >= this.pagesTotal) {
                startPage = this.pagesTotal - 9;
                endPage = this.pagesTotal;
            } else {
                startPage = this.currentPage - 5;
                endPage = this.currentPage + 4;
            }
        }

        this.pages = _.range(startPage, endPage + 1);
    }
}