import {Injectable} from '@angular/core';
import  * as findOrientation from '../../../node_modules/exif-orientation/index';



@Injectable()

export class RotateImageComponent {

    load(file, cb) {
        findOrientation(file, (err, orientation) => {
            if (!err) {
               this.loadImage(URL.createObjectURL(file),  (img) => {
                    if (img) {
                        cb(undefined, this.translate(img, orientation));
                    } else {
                        cb(new Error('Could not load image.'));
                    }
                });
            } else {
                cb(err);
            }
        });
    };

    translate(image, orientation) {
        orientation = orientation || {};
        //opts = opts || {};
        let c = document.createElement('canvas');
        let ctx = c.getContext('2d');
        // c.width = opts.width || image.naturalWidth;
        // c.height = opts.height || image.naturalHeight;
        if (orientation.rotate) {
            c.width = 150;
            c.height = 200;
        }else {
            c.width = 200;
            c.height = 150;
        }

        if (orientation.scale.x === -1 && orientation.scale.y === -1) {
            ctx.translate(c.width, c.height);
            ctx.scale(orientation.scale.x, orientation.scale.y);
        } else if (orientation.scale.x !== 1) {
            ctx.translate(c.width, 0);
            ctx.scale(orientation.scale.x, 1);
        } else if (orientation.scale.y !== 1) {
            ctx.translate(0, c.height);
            ctx.scale(1, orientation.scale.y);
        }
        if (orientation.rotate) {

            ctx.translate(c.width * 0.5, c.height * 0.5);
            ctx.rotate(orientation.rotate * (Math.PI / 180));
            ctx.translate(-c.width*0.5,-c.height*0.5);

        }
        if(orientation.rotate) {
            ctx.drawImage(image, -25, 25, c.height, c.width);
        } else {
            ctx.drawImage(image, 0,0,c.width,c.height);
        }

        return c;
    };

    loadImage(src, cb) {
        let img = new Image();
        img.onload =  () => {
            cb(img);
        };
        img.onerror = () => {
            cb();
        };
        img.src = src;
    }
 }

