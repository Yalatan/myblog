import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Subject} from "rxjs/internal/Subject";
import { map } from "rxjs/operators";

@Injectable()
export class ArticlesService {
  public onNewArticle = new Subject();
  public articlesOnPage = new Subject();
  public articlesByCategory = new Subject();

  constructor(private _http: HttpClient){}

    getArticles(currentPage, articlesPerPage) {
    const params = new HttpParams()
        .set('page', currentPage)
        .set('articlesPerPage', articlesPerPage);
      return this._http.get('http://localhost:3001/articles/list', {params: params}).pipe(
          map(res => {
            this.articlesOnPage.next(res);
            return res;
          })
      );
    }


    postArticle(article, imagesFiles, mainImageFile) {
      let formData:FormData = new FormData();
      formData.append('article', JSON.stringify(article));
      for (let i = 0; i < imagesFiles.length; i++) {
        formData.append("images", imagesFiles[i]);
      }
      formData.append('mainImage', mainImageFile);
      return this._http.post('http://localhost:3001/articles/new', formData).pipe(
          map(res => {
              this.onNewArticle.next(res);
          })
      )

    }

    getArticlesByCategory (category, currentPage, articlesPerPage) {
        const params = new HttpParams()
            .set('category', category)
            .set('page', currentPage)
            .set('articlesPerPage', articlesPerPage);
        return this._http.get('http://localhost:3001/articles/category', {params: params}).pipe(
            map(res => {
                this.articlesByCategory.next(res);
                return res;
            })
        );
    }

    getDetailArticle(id) {
        return this._http.get('http://localhost:3001/articles/' + id)
    }

}
