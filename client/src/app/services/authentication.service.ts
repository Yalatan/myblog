import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable()
export class AuthenticationService {
    constructor(private _http: HttpClient){}

    postLoginData(loginData) {
        return this._http.post('http://localhost:3001/', loginData)
    }

    postRegistrationData() {}
}