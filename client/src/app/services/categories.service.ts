import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CategoriesService {

    constructor(private _http: HttpClient){}

    getCategories() {
        return this._http.get('http://localhost:3001/categories/list')
    }


}