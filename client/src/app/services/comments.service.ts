import { Injectable } from '@angular/core';
// import {HttpClient} from "../../../node_modules/@angular/common/http";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CommentsService {
    constructor(private _http: HttpClient){}

    postComment(comment) {
        return this._http.post('http://localhost:3001/comments/new', comment)
    }

}