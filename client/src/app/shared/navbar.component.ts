import {Component, OnInit} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';

import {CategoriesService} from "../services/categories.service";
import {LoginComponent} from "../login/login.component";

@Component({
  selector: "navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ['./navbar.component.css']

})
export class NavbarComponent implements OnInit{
  categories;
  modalRef: BsModalRef;

  constructor (private _categoriesService: CategoriesService,
               private modalService: BsModalService) {}

    ngOnInit () {
        this._categoriesService.getCategories().subscribe(categories => {
            this.categories = categories;
        });

    }
    openModal() {
        this.modalRef = this.modalService.show(LoginComponent);
    }

}
