var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    categoryName: {type: String, require: true, ref: 'Category'},
    articleName: {type: String, require: true},
    content: {type: String, require: true},
    created: {type: Date},
    imageLinks: [{type: String}],
    mainImageLink: {type: String}
});

module.exports = mongoose.model('Article', schema);