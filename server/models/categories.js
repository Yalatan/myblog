var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let Category;
let schema = new Schema({
    categoryName: {type: String},
    categoryNameEn: {type: String}
});

schema.statics.initCategory = (Category) => {
    let _categories = [
        {'categoryName': 'Здоровье', 'categoryNameEn': 'health', '_v': 1},
        {'categoryName': 'Интересы', 'categoryNameEn': 'hobbies', '_v': 1},
        {'categoryName': 'Всякое', 'categoryNameEn': 'others', '_v': 1}
    ];

    Category.remove({}, (err) => {
        _categories.forEach(category => {
            Category.create(category);
        });
       })
}


module.exports = mongoose.model('Category', schema);
