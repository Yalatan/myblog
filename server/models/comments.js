const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let schema = new Schema({
    userName: {type: String},
    commentText: {type: String, require: true},
    articleToComment: {type: String, require: true}
});

module.exports = mongoose.model('Comment', schema);