const express = require('express');
const router = express.Router();
const fs = require("fs");
const mongoose = require("mongoose");
const multer  = require('multer');
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
const s3 = new AWS.S3();
const upload = multer({ dest: 'uploads/'
                        // limits: {
                        //     fileSize: 2 * 1024 * 1024,
                        // }
});

let Article = require('../models/articles');
let Category = require('../models/categories');


router.get('/list', function(req, res, next) {
    let articlesPerPage = parseInt(req.query.articlesPerPage);
    let pageNum = parseInt(req.query.page);
    Article.find({})
        .sort('-created')
        .skip(articlesPerPage * (pageNum - 1))
        .limit(articlesPerPage)
        .exec(function(err, articles) {
           Article.countDocuments({}, function(err, articlesTotal) {
               let result = {
                   articles: articles,
                   articlesTotal: articlesTotal
               };
               res.send(result);
           });

        })
});


router.post('/new', upload.any(), function(req, res, next) {
    let files = req.files;

    let fields = JSON.parse(req.body.article);
    let article = new Article({
        categoryName: fields.categoryName,
        articleName: fields.articleName,
        content: fields.content,
        created: new Date()
    });

    article.save().then((result, err) => {
        console.log("File:", files);
        let promises= [];
        for (let i = 0; i < files.length; i++) {

            function saveImagesToS3 () {
                const filename = result._id + '/'  + files[i].fieldname + '/' + (new Date()).getTime() + '_' + files[i].filename;
                const params = {Bucket: 'imagesforblog',
                    Key: filename,
                    Body: fs.createReadStream(files[i].path),
                    ACL: 'public-read',
                    ContentType: 'image/jpeg'
                };
                return new Promise((resolve, reject) => {
                    s3.upload(params,  (err, data) => {
                        fs.unlink(files[i].path, (err) => {
                            if (err) throw err;
                        });

                        if (files[i].fieldname === 'images') {
                            result.imageLinks.push(data.Location);
                        }
                        if (files[i].fieldname === 'mainImage') {
                            result.mainImageLink = data.Location;
                        }
                        if (err) {
                            reject(console.log('Error uploading data: ', err));
                        } else {
                            resolve(console.log('successfully uploaded the image!', data));
                        }
                    })

                });
            }
            promises.push(saveImagesToS3());
        }

        Promise.all(promises).then((result, err) => {
          article.save();

        if (err) {
            res.status(404).json({
                title: 'An error has occurred',
                error: err
            });
            console.log('Error uploading data: ', data);

        } else {
            res.status(201).json({
                message: 'Saved article',
                obj: result
            });
        }
       }).catch(error => {
            console.log(error);
        });

    });
});


router.get('/category', function(req, res, next) {
    let category = req.query.category;
    let articlesPerPage = parseInt(req.query.articlesPerPage);
    let pageNum = parseInt(req.query.page);
    Category.find({categoryNameEn: category}, function(err, category) {
      Article.find({categoryName: category[0].categoryName})
          .select('-imageLinks')
          .sort('-created')
          .skip(articlesPerPage * (pageNum - 1))
          .limit(articlesPerPage)

          .exec(function(err, articles) {
                Article.countDocuments({categoryName: category[0].categoryName}, function(err, articlesTotal) {
                    let result = {
                        articles: articles,
                        articlesTotal: articlesTotal
                    };
                    if (err) {
                        res.status(404).json({
                            title: 'An error has occurred',
                            error: err
                        });
                        console.log('Error uploading data: ', data);

                    } else {
                        res.send(result);
                    }
                });

      });
    });

});


router.get('/:id', function(req, res, next) {
   let articleId = req.params.id;
   Article.findById({_id: articleId}, function(err, article) {
       if (err) {
           res.status(404).json({
               title: 'An error has occurred',
               error: err
           });
           console.log('Error uploading data: ', data);

       } else {
           res.send(article);
       }
   })
});



module.exports = router;
