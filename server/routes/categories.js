var express = require('express');
var router = express.Router();

var Categories = require('../models/categories');

router.get('/list', function(req, res, next) {
    Categories.find({}, function(err, categories) {
        res.send(categories);
    })
});


module.exports = router;